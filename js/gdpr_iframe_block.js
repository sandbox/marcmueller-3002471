(function($, Drupal, drupalSettings) {
  Drupal.behaviors.yourbehavior = {
    attach: function (context, settings) {
       var iframeid = '#'+drupalSettings.gdpr_iframe_block.gdpr_iframe_block.iframeid;
       var source = drupalSettings.gdpr_iframe_block.gdpr_iframe_block.source;
       
       jQuery(".gdpr-open-link").click(function () {
         jQuery(iframeid).show();
         jQuery(iframeid).attr("src", source);
         jQuery(this).parent().hide();
       });
    }
  };
})(jQuery, Drupal, drupalSettings);