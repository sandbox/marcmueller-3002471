<?php

/**
 * @file
 */
namespace Drupal\gdpr_iframe_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Block(
 * id = "gdpr_iframe_block",
 * admin_label = @Translation("GDPR Iframe block"),
 * )
 */
class gdpriframeBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $config = $this->configuration;
        $defaults = $this->defaultConfiguration();
        $form = parent::blockForm($form, $form_state);
        
        $form['source'] = [
            '#type' => 'url',
            '#title' => $this->t('<strong>Iframe source URL</strong>'),
            '#default_value' => $config['source'],
            '#description' => $this->t('Enter the Iframe source url here.'),
            '#required' => TRUE,
        ];
        
        $form['description'] = [
            '#type' => 'textarea',
            '#title' => $this->t('<strong>GDPR description text</strong>'),
            '#default_value' => $config['description'],
            '#description' => $this->t('Enter your description text here.'),
            '#required' => TRUE,
        ];
        
        $form['linktext'] = [
            '#type' => 'textfield',
            '#title' => $this->t('<strong>Link text</strong>'),
            '#default_value' => $config['linktext'],
            '#description' => $this->t('Link text'),
            '#required' => TRUE,
        ];
        
        $form['iframeid'] = [
            '#type' => 'machine_name',
            '#title' => $this->t('<strong>Iframe ID</strong>'),
            '#default_value' => $config['iframeid'],
            '#description' => $this->t('Enter the Iframe ID here.'),
            '#required' => TRUE,
        ];
        
        $form['iframe_dimension_x'] = [
            '#type' => 'number',
            '#title' => $this->t('<strong>Iframe width in px</strong>'),
            '#default_value' => $config['iframe_dimension_x'],
            '#description' => $this->t('Iframe width in px.'),
            '#required' => TRUE,
        ];
        
        $form['iframe_dimension_y'] = [
            '#type' => 'number',
            '#title' => $this->t('<strong>Iframe height in px</strong>'),
            '#default_value' => $config['iframe_dimension_y'],
            '#description' => $this->t('Iframe height in px.'),
            '#required' => TRUE,
        ];
        
        
        return $form;
    }
    
    
    /**
     * {@inheritdoc}
    */
    public function defaultConfiguration() {
        return [
        'source' => 'https://example.com',
        'description' => 'By clicking here you are agreeing to the Terms and Conditions...',
        'linktext' => 'Open',
        'iframeid' => 'Iframe ID',
        'iframe_dimension_x' => 880,
        'iframe_dimension_y' => 360,
        ];
    }
    
    
    
    
    /**
     * block submit actions
     * {@inheritdoc}
    */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $this->configuration['source'] = $form_state->getValue('source');
        $this->configuration['description'] = $form_state->getValue('description');
        $this->configuration['linktext'] = $form_state->getValue('linktext');
        $this->configuration['iframeid'] = $form_state->getValue('iframeid');
        $this->configuration['iframe_dimension_x'] = $form_state->getValue('iframe_dimension_x');
        $this->configuration['iframe_dimension_y'] = $form_state->getValue('iframe_dimension_y');
    }
        
    
    
    
    /**
    * {@inheritdoc}
    */
    public function build() {  
        $source = $this->configuration['source'];
        $description = $this->configuration['description'];
        $linktext = $this->configuration['linktext'];
        $iframeid = $this->configuration['iframeid'];
        $iframe_dimension_x = $this->configuration['iframe_dimension_x'];
        $iframe_dimension_y = $this->configuration['iframe_dimension_y'];
        
        $markup = "<div class='gdpr-frame'>";
        $markup .= "<div class='gdpr-desc'>".$description."</div>";
        $markup .= "<a class='gdpr-open-link'>".$linktext."</a>";
        $markup .= "</div>";
        $markup .= "<iframe class='gdpr-iframe' id='".$iframeid."' frameborder='0' scrolling='no' width='".$iframe_dimension_x."' height='".$iframe_dimension_y."'></iframe>";
        
        return array (
            '#type' => 'markup',
            '#children' => $markup,
            '#cache' => array(
                'max-age' => 0,
            ),
            '#attached' => array(
                'library' => array(
                    'gdpr_iframe_block/gdpr_iframe_block',
                ),
                'drupalSettings' => array(
                    'gdpr_iframe_block' => array(
                        'gdpr_iframe_block' => array(
                            'iframeid' => $iframeid,
                            'source' => $source,
                        ),
                    ),
                ),
            ),
        );
        
        
    }
}